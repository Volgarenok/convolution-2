#ifndef DSPDATA_HPP
#define DSPDATA_HPP
namespace dsp
{
  namespace data
  {
    namespace H
    {
      constexpr auto rows = 8u;
      constexpr auto columns = 8u;
    }
    namespace X
    {
      constexpr auto rows = 8u;
      constexpr auto columns = 8u;
    }
    namespace Y
    {
      constexpr auto rows = H::rows + X::rows - 1;
      constexpr auto columns = H::columns + X::columns - 1;
    }
  }
}
#endif
