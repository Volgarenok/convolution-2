#include <iostream>
#include <fstream>
#include "dspdefs.hpp"
#include "dspdata.hpp"

int main(void)
{
  using namespace dsp::data;
  auto x = new int[X::rows * X::columns]();
  auto h = new int[H::rows * H::columns]();
  auto Xm = dsp::matrix< X::rows, X::columns, int >{x};
  auto Hm = dsp::matrix< H::rows, H::columns, int >{h};
  auto input_x = std::ifstream("input_fixed_x.txt");
  auto input_h = std::ifstream("input_fixed_h.txt");
  dsp::read_data< X::rows, X::columns, int >(input_x, Xm);
  dsp::read_data< H::rows, H::columns, int >(input_h, Hm);
  std::cout << Xm << std::endl;
  std::cout << Hm << std::endl;
  int * y  = new int[Y::rows * Y::columns]();
  auto Ym = dsp::matrix< Y::rows, Y::columns, int >{y};

  for(auto i = 0u; i < Y::rows; i++)
  {
    for(auto j = 0u; j < Y::columns; j++)
    {
      for(auto k1 = 0u; k1 < X::rows; k1++)
      {
	for(auto k2 = 0u; k2 < X::columns; k2++)
	{
	  if((i - k1 < H::rows) && (j - k2 < H::columns))
	  {
	    Ym[i][j] += Xm[k1][k2] * Hm[i - k1][j - k2];
	  }
	}
      }
    }
  }
  auto output = std::ofstream("method_1_y.txt");
  output << Ym << std::endl;
  delete [] y;
  return 0;
}
