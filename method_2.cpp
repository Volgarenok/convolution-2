#include <iostream>
#include <fstream>
#include "dspdefs.hpp"
#include "dspdata.hpp"

int main(void)
{
  using namespace dsp::data;
  auto x = new int[X::rows * X::columns]();
  auto h = new int[H::rows * H::columns]();
  auto Xm = dsp::matrix< X::rows, X::columns, int >{x};
  auto Hm = dsp::matrix< H::rows, H::columns, int >{h};
  auto input_x = std::ifstream("input_fixed_x.txt");
  auto input_h = std::ifstream("input_fixed_h.txt");
  dsp::read_data< X::rows, X::columns, int >(input_x, Xm);
  dsp::read_data< H::rows, H::columns, int >(input_h, Hm);
  std::cout << Xm << std::endl;
  std::cout << Hm << std::endl;
  auto y  = new int[Y::rows * Y::columns]();
  auto Ym = dsp::matrix< Y::rows, Y::columns, int >{y};
  auto output = std::ofstream("method_2_temp.txt");
  for(auto k1 = 0u; k1 < X::rows; k1++)
  {
    for(auto k2 = 0u; k2 < X::columns; k2++)
    {
      auto temp = new int[Y::rows * Y::columns]();
      auto Tempm = dsp::matrix< Y::rows, Y::columns, int >{temp};
      for(auto m1 = 0u; m1 < H::rows; m1++)
      {
	for(auto m2 = 0u; m2 < H::columns; m2++)
	{
	  Tempm[m1 + k1][m2 + k2] = Hm[m1][m2] * Xm[k1][k2];
	}
      }
      Ym += Tempm;
      output << "Temp: (" << k1 << "," << k2 << ")" << std::endl;
      output << Tempm << std::endl;
      delete [] temp;
    }
  }
  output = std::ofstream("method_2_y.txt");
  output << Ym;
  delete [] y;
  return 0;
}
